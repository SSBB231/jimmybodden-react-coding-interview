import { Box, Button, Input, useTheme } from '@mui/material';
import { ReactNode, useState } from 'react';

export type InlineEditInputProps = {
  value: string;
  setValue: (value: string) => void;
};

export type InlineEditStates = 'view' | 'edit';

export const InlineEditInput: React.FC<InlineEditInputProps> = ({
  value,
  setValue,
}) => {
  const theme = useTheme();
  const [currentState, setCurrentState] = useState<InlineEditStates>('view');
  const [currentValue, setCurrentValue] = useState<string>(value);

  const RENDER_ITEMS: Record<InlineEditStates, ReactNode> = {
    view: (
      <Input readOnly value={value} onClick={() => setCurrentState('edit')} />
    ),
    edit: (
      <Box>
        <Input
          value={currentValue}
          onChange={(event) => setCurrentValue(event.target.value)}
        />
        <Box display="flex" flexDirection="row" gap={theme.spacing(2)}>
          <Button
            onClick={() => {
              setValue(currentValue);
              setCurrentState('view');
            }}
          >
            Yes
          </Button>
          <Button
            onClick={() => {
              setCurrentState('view');
              setCurrentValue(value);
            }}
          >
            No
          </Button>
        </Box>
      </Box>
    ),
  };

  const renderItem = RENDER_ITEMS[currentState];

  return (
    <Box display="flex" flexDirection="column">
      {renderItem}
    </Box>
  );
};
